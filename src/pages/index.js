import React, { Component } from "react"
import { Link } from "gatsby"
import axios from "axios"

import Layout from "../components/layout"

class IndexPage extends Component {
  
  // state = {
  //   loading: false,
  //   error: false,
  //   osrmData: {
  //   }
  // }

  // componentDidMount() {
  //   this.fetchOSRM()
  // }
  
  render () {
    return (
      <Layout>
      <pre>
        <b>
          Max Ikäheimo - maxika@uef.fi
          </b>
        <br/>
        <Link to="/map">
          See the map
        </Link>
        <br/>
        <br/>
        <a href="https://gitlab.com/IKIUM/lami">
          https://gitlab.com/IKIUM/lami
        </a>
        <br/>
        <b>
          Python
        </b>
        <br></br>
        <b>
          Flask framework <a href="http://flask.pocoo.org/">Docs</a>
        </b>
        <br></br>
        <b>Python libraries (requests)</b>
        
      </pre>

      <h1>1.1 Get A to B using OSRM. Try using multiple transportation modes.</h1>
      <pre>

        <li>
          <a href="http://project-osrm.org/docs/v5.15.2/api/#general-options">OSRM API Documentation</a>
        </li>
      </pre>
      <p>
        First we fetch a route from my house to the nearest pub using project-osrm using the osrm API.
        Simple HTTP GET request is enough. Just replace <b>'driving'</b> with walking or other options from OSRM API to get routes for different transportation modes.
      </p>
      <pre style={{wordWrap: 'break'}}>
        http://router.project-osrm.org/route/v1/driving/62.890712,27.6894213;62.888537,27.6293233?alternatives=false&steps=true&geometries=geojson&overview=simplified&annotations=true
      </pre>
      <h1>1.2 Get buildings using the Overpass API</h1>
        <pre>

        <li>
          <a href="https://wiki.openstreetmap.org/wiki/Overpass_API">Overpass API docs</a>
        </li>
        </pre>
      <p>
        Then we fetch the buildings in a bounding box that covers the area of the routes to the nearest pub using the
        overpass API with an XML-request.
      </p>
      <pre>
        <b>
          XML form data:
        </b>
        <br></br>
        See gitlab code for the request implementation: <a href="https://gitlab.com/IKIUM/lami">
          https://gitlab.com/IKIUM/lami
        </a>
        
      </pre>
      {/* <p>
      The HTTP request is done by axios (node module), which allows HTTPS requests on run the client. The example is running on 
        Gatsby JS which uses React to render static content but can be extended to use Reacts full capabilities. In this case,
        Reacts methods are used to execute the client side asynchronous API call with axios.
      </p> */}
      <h1>1.3 Compute shortest path avoiding buildings using a visibility graph. (not yet implemented)</h1>
      <p>
        TaipanRex's pyvisgraph python library is going to be used to compute the visibility graph. Implementation done later.
      </p>
      <pre>

        <li>
          <a href="https://github.com/TaipanRex/pyvisgraph">https://github.com/TaipanRex/pyvisgraph</a>
        </li>
      </pre>
      <p>
        Pass to OSRM data and Overpass building data to the librarys functions to get the visibility graph and the shortest 
        route. To be implemented.
      </p>
      <h1>1.4 Compare which is more similar to reality: straight line, building avoidance or road network?</h1>
      <p>
        Use the Similarity API to compare which route is most similar to reality. Not yet implemented.
      </p>
      <pre>
        <li>
          <a href="http://cs.uef.fi/mopsi/routes/similarityApi/doc.html">http://cs.uef.fi/mopsi/routes/similarityApi/doc.html</a>
        </li>
      </pre>
      <p>
        Pass route data to the Similarity API and compare the results programmatically. Not yet implemented.
      </p>
      {/* <h2>The api params</h2>
      <div>
        <pre>{JSON.stringify(this.state.osrmData.routes) }</pre>
      </div> */}
      </Layout>
    )  
  }

  // fetchOSRM = () => {
  //   this.setState({ loading: true })
  //   axios
  //     .get('http://router.project-osrm.org/route/v1/car/62.890712,27.6894213;62.888537,27.6293233?alternatives=false&steps=true&geometries=geojson&overview=simplified&annotations=true')
  //     .then(response => {
  //       console.log('OSRM data: ', response)
  //       this.setState({
  //         loading: false,
  //         osrmData: response.data
  //       })
  //     })
  //     .catch(error => {
  //       this.setState({ loading: false, error })
  //     })
  //   }
}

export default IndexPage

