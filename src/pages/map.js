import React, { Component } from "react"
import { Link } from "gatsby"
import ReactMapGL from 'react-map-gl';
import { fromJS } from 'immutable';
import axios from 'axios';
import Layout from "../components/layout"
import './map.css'
import config from '../../data/SiteConfig'

class Map extends Component {

    state = {
        viewport: {
            width: 800,
            height: 400,
            latitude: config.mapLat,
            longitude: config.mapLon,
            zoom: 12
        },
        loading: false,
        error: false,
        routes: [],
        shortestPath: [],
        routeColors: ['red', 'blue', 'yellow']
    };

    render() {
        return (
            <Layout>
                <h1>Map</h1>
                <Link to="/">Back to project description</Link>
                <p>This is where the mapbox will be rendered</p>
                <ul>
                    <li><span style={{color: 'green'}}>green:</span> vigraph shortest path</li>
                    <li><span style={{color: 'red'}}>red:</span> osm_c / Car</li>
                    <li><span style={{color: 'blue'}}>blue:</span> osm_w / Walk</li>
                    <li><span style={{color: 'yellow'}}>yellow:</span> osm_b / Bike</li>
                </ul>
                <ReactMapGL
                    ref={(reactMap) => { this.reactMap = reactMap; }}
                    {...this.state.viewport}
                    onViewportChange={(viewport) => this.setState({ viewport })}
                    mapboxApiAccessToken='pk.eyJ1IjoiaWtpdW0iLCJhIjoiY2pza20xZnU4MTIwNDN5bjVjZG56N3BpdCJ9.K4mOFi_woHQFcyYHXr3Wlw'
                // mapStyle={mapStyle}
                />
            </Layout>
        )
    }

    componentDidMount() {
        this.fetchRoute('osm_c', config.slat, config.slon,
            config.elat, config.elon)
        this.fetchRoute('osm_w', config.slat, config.slon,
            config.elat, config.elon)
        this.fetchRoute('osm_b', config.slat, config.slon,
            config.elat, config.elon)
        // for demo purposes define payload here. If this was a real app
        // passing the data from this point allows fetching on actions/events etc
        var vgPayload = {
            slat: config.slat,
            slon: config.slon,
            elat: config.elat,
            elon: config.elon,
            north: config.north,
            south: config.south,
            west: config.west,
            east: config.east
        }
        this.fetchShortestPath(vgPayload)
        const map = this.reactMap.getMap();
        map.on('load', () => {
            map.addLayer({
                "id": "shortestPath",
                "type": "line",
                "source": {
                    "type": "geojson",
                    "data": {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "LineString",
                            "coordinates": this.state.shortestPath
                        }
                    }
                },
                "layout": {
                    "line-join": "round",
                    "line-cap": "round"
                },
                "paint": {
                    "line-color": "green",
                    "line-width": 8
                }
            })
            this.state.routes.forEach((route, index) => {
                map.addLayer({
                    "id": "route" + index,
                    "type": "line",
                    "source": {
                        "type": "geojson",
                        "data": {
                            "type": "Feature",
                            "properties": {},
                            "geometry": {
                                "type": "LineString",
                                "coordinates": route
                            }
                        }
                    },
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": this.state.routeColors[index],
                        "line-width": 8
                    }
                })
            })

        })
    }

    // fetch route from proxy
    fetchRoute = (method, slat, slon, elat, elon) => {
        this.setState({ loading: true })
        var queryString = '?method=' + method
            + '&slon=' + slon
            + '&slat=' + slat
            + '&elat=' + elat
            + '&elon=' + elon
        axios.get(config.endpoint + '/routes' + queryString).then(response => {
            var decoded = this.decodeGeometry(response.data[0]['geometry'])
            var coordinates = this.parseRouteCoordinates(decoded)
            this.setState({
                loading: false,
                routes: [...this.state.routes, coordinates]
            })
        }).catch(error => {
            this.setState({ loading: false, error })
        })
    }

    // fetch shortest path from proxy
    fetchShortestPath = (payload) => {
        console.log('fetching')
        this.setState({ loading: true })
        axios.post(config.endpoint + '/vigraph', payload).then(response => {
            var coordinates = this.parseVigraphCoordinates(response.data)
            console.log('response')
            this.setState({
                loading: false,
                shortestPath: coordinates
            })
        }).catch(error => {
            this.setState({ loading: false, error })
        })
    }

    // parse coordinates for geojson rendering
    parseVigraphCoordinates(coordinates) {
        var parsed = []
        coordinates.forEach(coordinate => {
            let c = []
            c.push(coordinate.lon)
            c.push(coordinate.lat)
            parsed.push(c)
        })
        return parsed
    }

    parseRouteCoordinates(coordinates) {
        var parsed = []
        coordinates.forEach(coordinate => {
            let c = []
            c.push(coordinate.lng)
            c.push(coordinate.lat)
            parsed.push(c)
        })
        return parsed
    }

    // decoder from the project example doc
    decodeGeometry(encoded) {
        var points = []
        var index = 0,
            len = encoded.length;
        var lat = 0,
            lng = 0;
        while (index < len) {
            var b, shift = 0,
                result = 0;
            do {
                b = encoded.charAt(index++).charCodeAt(0) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++).charCodeAt(0) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            points.push({
                lat: (lat / 1E5),
                lng: (lng / 1E5)
            })
        }
        return points
    }
}

export default Map