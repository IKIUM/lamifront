module.exports = {
    endpoint: 'http://localhost:5000',
    // endpoint: 'https://thawing-dawn-30114.herokuapp.com',
    // Test input
    slat: 62.60615537467424,
    slon: 29.758137686352484,
    elat: 62.597861090111564,
    elon: 29.732388479809515,
    mapLat: 62.597861090111564,
    mapLon: 29.732388479809515,
    north: 62.607165,
    south: 62.589568,
    west: 29.734583,
    east: 29.787111
    // north: 62.600281996255,
    // south: 62.598160739286,
    // west: 29.760421577154,
    // east: 29.764547634562
}